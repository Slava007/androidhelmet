package com.example.slava.helmethird;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


import java.util.Random;

public class GpsActivity extends FragmentActivity implements OnMapReadyCallback {

    GoogleMap map;
    Button click_cheng;

    double n[] = {49.857856,49.840769,49.856122,49.858254};
    double m[] = {24.026101,24.027287,24.021260, 24.036210};

    int c =0;
    int k =0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        click_cheng = (Button)findViewById(R.id.button);
    }


    public void openInfoActivity (View view){
        Intent intent = new Intent(this,InfoActivity.class);
        startActivity(intent);
    }

    public void RandomClick (View view){
        Random i = new Random();
        c = i.nextInt(4 - 1)+1;
        k = i.nextInt(4 - 1)+1;

        map.clear();
        LatLng Test = new LatLng(n[c], m[k]);
        map.addMarker(new MarkerOptions().position(Test).title("Test"));
        map.moveCamera(CameraUpdateFactory.newLatLng(Test));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        LatLng Test = new LatLng(n[c], m[k]);
        map.addMarker(new MarkerOptions().position(Test).title("Test"));
        map.moveCamera(CameraUpdateFactory.newLatLng(Test));

    }



}