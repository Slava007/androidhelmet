package com.example.slava.helmethird;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void openInfoActivity (View view){
        Intent intent = new Intent(this,InfoActivity.class);
        startActivity(intent);
    }

    public void openGpsActivity (View view){
        Intent intent = new Intent(this,GpsActivity.class);
        startActivity(intent);
    }

    public void openBluetootheActivity (View view){
        Intent intent = new Intent(this,BluetootheActivity.class);
        startActivity(intent);
    }

    public void openSettingsActivity (View view){
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void openProtectActivity (View view){
        Intent intent = new Intent(this,ProtectActivity.class);
        startActivity(intent);
    }

    public void openBatterryActivity (View view){
        Intent intent = new Intent(this,BatteryActivity.class);
        startActivity(intent);
    }

    public void openTimeActivity (View view){
        Intent intent = new Intent(this,TimeActivity.class);
        startActivity(intent);
    }
}
