package com.example.slava.helmethird;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class BluetootheActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetoothe);
    }

    public void openInfoActivity (View view){
        Intent intent = new Intent(this,InfoActivity.class);
        startActivity(intent);
    }
}